import React, { Component, PropTypes } from 'react';
import styled from 'styled-components';

const Root = styled.div`
  --base-column: 8.333333%;
  --base-padding: 0 15px;
`;

const Col = styled.div`
  @media (max-width: 710px) {
    width: calc(${props => props.sm} * var(--base-column));
  }
  @media (min-width: 710px) and (max-width: 1030px) {
    width: calc(${props => props.md} * var(--base-column));
  }
  @media (min-width: 1030px) {
    width: calc(${props => props.lg} * var(--base-column));
  }
  padding: var(--base-padding);
`;

class Column extends Component {
  render() {
    return (
      <Root>
        <Col { ...this.props }>
          { this.props.children }
        </Col>
      </Root>
    );
  }
}

Column.propTypes = {
  sm: PropTypes.number,
  md: PropTypes.number,
  lg: PropTypes.number
};

export default Column;
