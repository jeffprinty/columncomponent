# Column Component

Responsive layout column component using `styled-components`

```
import Column from 'Shared/Column';
...
<div>
	<Column sm={ 12 } md={ 9 } lg={ 9 }>
	{/* Column Content */}
	</Column>
	<Column sm={ 12 } md={ 3 } lg={ 3 }>
	{/* Column Content */}
	</Column>
</div>
```
